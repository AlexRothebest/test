import uuid

from sqlalchemy.dialects.postgresql import UUID

from app import db


class Symbol(db.Model):

    __tablename__ = 'symbols'

    id = db.Column(UUID(), primary_key=True)

    symbol = db.Column(db.String(20))
    name = db.Column(db.String(100))

    def __init__(self, symbol, name):
        self.id = uuid.uuid4().urn
        self.symbol = symbol
        self.name = name

    @property
    def json(self):
        return {
            'symbol': self.symbol,
            'name': self.name
        }