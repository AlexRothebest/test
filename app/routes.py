from app import app
from app.models import Symbol

from flask import request, render_template


@app.route('/', methods=['GET', 'POST'])
def main():
    return render_template('index.html')


@app.route('/find_similar_assets', methods=['POST'])
def find_similar_assets():
    data = request.get_json(force=True)

    if 'search_value' not in data:
        return {
            'success': False,
            'message': 'Missing search_value field'
        }

    search_value = data['search_value']
    if not search_value:
        return {
            'success': False,
            'message': 'Search value is empty'
        }

    matching_symbols = [symbol.json for symbol in Symbol.query.filter(Symbol.symbol.contains(search_value))]

    return {
        'success': True,
        'matching_symbols': matching_symbols
    }
