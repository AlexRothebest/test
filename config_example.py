HOST = 'host'
PORT = 80

DEBUG = False

SECRET_KEY = 'secret key'

DATABASE_URI = 'somesql://user:password@host:port/database'

PYEX_API_TOKEN = 'api token'
