from app import db
from app.models import Symbol
from config import PYEX_API_TOKEN

import pyEX as pyex

print('Start extracting symbols by API')

client = pyex.Client(api_token=PYEX_API_TOKEN, version='sandbox')

print('Extracted symbols successfully')

print('Start writing symbols to the database')

for symbol_data in client.symbols():
    new_symbol = Symbol(
        symbol=symbol_data['symbol'],
        name=symbol_data['name']
    )
    db.session.add(new_symbol)

db.session.commit()

print('Symbols has been added to the database')
