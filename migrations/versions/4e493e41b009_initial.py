"""Initial

Revision ID: 4e493e41b009
Revises: 
Create Date: 2020-12-18 16:54:27.599878

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '4e493e41b009'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('symbols',
    sa.Column('id', postgresql.UUID(), nullable=False),
    sa.Column('symbol', sa.String(length=20), nullable=True),
    sa.Column('name', sa.String(length=100), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('symbols')
    # ### end Alembic commands ###
